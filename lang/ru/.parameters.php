<?
$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_IBLOCK"] = "Инфоблок";
$MESS["IBLOCK_ELEMENT_ID"] = "ID елементов инфоблока";
$MESS["IBLOCK_SECTION_ID"] = "ID разделов инфоблока";
$MESS["ELEMENT_COUNT"] = "Максимальное количество элементов";
$MESS["ELEMENT_SORT_FIELD"] = "Сортировать по полю";
$MESS["ELEMENT_SORT_ORDER"] = "Порядок сортировки";
$MESS["ELEMENT_NAME"] = "Показывать имя элемента";
$MESS["ELEMENT_PRICE"] = "Показывать цену элемента";
$MESS["ELEMENT_ANOUNCE"] = "Показывать анонс элемента";
$MESS["ELEMENT_COUNT_ON_PAGE"] = "Количество выводимых элементов";
$MESS["USE_SECTION_ID"] = "Показать элементы из раздела";
$MESS["USE_ELEMENT_ID"] = "Показать определенные элементы";
$MESS["USE_IBLOCK_ID"] = "Показать элементы инфоблока";
$MESS["SOURCE"] = "Источник данных";
$MESS["USE_FRACTIONAL_VALUE"] = "Использовать дробное значение цены";
$MESS["HEIGHT_WRAP"] = "Высота обертки для изображения";
$MESS["INCLUDE_SUBSECTIONS"] = "Показывать элементы подразделов";
$MESS["PICTURE_FROM"] = "Брать картинку из";
$MESS["SCROLL_SPEEP"] = "Скорость прокрутки";
$MESS["AUTO_SCROLL"] = "Включить автопрокрутку";
?>